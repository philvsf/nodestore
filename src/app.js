'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const router = express.Router();

mongoose.connect('mongodb://balta:balta@ds028310.mlab.com:28310/ndstr87');

const Product = require('./models/product')

//carrega as rotas
const indexRoute = require('./routes/index-route');
const productRoute = require('./routes/product-route');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ 
	extended: false
}));

app.use('/', indexRoute);
app.use('/products', productRoute);

module.exports = app;